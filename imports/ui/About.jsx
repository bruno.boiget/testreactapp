import React from "react";
import Paper from "@material-ui/core/Paper";
import BaseComponent from "./BaseComponent.jsx";
import i18n from "meteor/universe:i18n";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  about: {
    padding: theme.spacing(3, 2)
  }
});

class About extends BaseComponent {
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Paper className={classes.about}>
          <Typography variant="h5" component="h1" align="center">
            {i18n.__("components.About.about")}
          </Typography>
          <Typography component="p" align="center">
            {i18n.__("components.About.aboutContent")}
          </Typography>
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(About);
