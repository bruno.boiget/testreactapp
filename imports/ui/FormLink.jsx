import React from "react";
import BaseComponent from "./BaseComponent.jsx";
import { Meteor } from "meteor/meteor";
import i18n from "meteor/universe:i18n";
import { withTracker } from "meteor/react-meteor-data";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";

import { insertLink } from "../api/methods";

const styles = theme => ({
  "@global": {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  }
});

class FormLink extends BaseComponent {
  constructor(props) {
    super(props);
    this.addLink = this.addLink.bind(this);
    this.state = Object.assign(this.state, { showDialog: false });
    this.dialogOpen = this.dialogOpen.bind(this);
    this.dialogClose = this.dialogClose.bind(this);
  }

  dialogOpen() {
    this.setState({ showDialog: true });
  }

  dialogClose() {
    this.setState({ showDialog: false });
  }

  render() {
    const { classes } = this.props;

    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        {this.props.currentUser ? (
          <div className={classes.paper}>
            <Button color="primary" onClick={this.dialogOpen}>
              {i18n.__("components.FormLink.addCard")}
            </Button>
            <Dialog
              open={this.state.showDialog}
              onClose={this.dialogClose}
              aria-labelledby="form-dialog-title"
            >
              <DialogTitle id="form-dialog-title">
                {i18n.__("components.FormLink.addCard")}
              </DialogTitle>
              <DialogContent>
                <form className={classes.form} noValidate>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="title"
                    label={i18n.__("components.FormLink.title")}
                    name="title"
                    autoComplete="title"
                    autoFocus
                    inputRef={c => {
                      this.textTitle = c;
                    }}
                  />
                  <TextField
                    id="description"
                    label={i18n.__("components.FormLink.description")}
                    name="description"
                    required
                    multiline
                    fullWidth
                    margin="normal"
                    variant="outlined"
                    inputRef={c => {
                      this.textDescription = c;
                    }}
                  />
                  <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="url"
                    label={i18n.__("components.FormLink.imageUrl")}
                    name="url"
                    autoComplete="url"
                    inputRef={c => {
                      this.textUrl = c;
                    }}
                  />
                </form>
              </DialogContent>
              <DialogActions>
                <Button onClick={this.dialogClose} color="primary">
                  {i18n.__("components.FormLink.cancelButton")}
                </Button>
                <Button onClick={this.addLink} color="primary">
                  {i18n.__("components.FormLink.addButton")}
                </Button>
              </DialogActions>
            </Dialog>
          </div>
        ) : (
          ""
        )}
      </Container>
    );
  }

  addLink = () => {
    title = this.textTitle.value.trim();
    description = this.textDescription.value.trim();
    url = this.textUrl.value.trim();

    // insert in database
    insertLink.call({ title, description, url }, (err, res) => {
      if (err) {
        console.log(err);
        if (err.error === "validation-error") {
          Bert.alert(err.reason, "danger", "growl-top-right");
          return;
        }
        if (err.error === "Links.methods.not-logged-in") {
          Bert.alert(
            i18n.__("components.FormLink.notifyNotLoggedin"),
            "danger",
            "growl-top-right"
          );
          return;
        }
      } else {
        Bert.alert(
          i18n.__("components.FormLink.notifySuccess"),
          "success",
          "growl-top-right"
        );
        this.textTitle.value = "";
        this.textDescription.value = "";
        this.textUrl.value = "";
        this.dialogClose();
      }
    });

    // Meteor.call('links.insert', title, description, url, (err, res) => {
    //     if (err) {
    //         Bert.alert('Veuillez remplir tous les champs obligatoires.', 'danger', 'growl-top-right');
    //     } else {
    //         Bert.alert('Carte Ajoutée', 'success', 'growl-top-right');
    //         this.textTitle.value="";
    //         this.textDescription.value="";
    //         this.textUrl.value="";
    //         this.dialogClose();
    //     }
    // });
  };
}

const FormLinkWithTracker = withTracker(() => {
  return {
    currentUser: Meteor.user()
  };
})(FormLink);
const FormLinkWithStyles = withStyles(styles)(FormLinkWithTracker);
export { FormLinkWithStyles as FormLink };
