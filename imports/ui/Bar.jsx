import React from "react";
import BaseComponent from "./BaseComponent.jsx";
import i18n from "meteor/universe:i18n";
import { Meteor } from "meteor/meteor";
import { withTracker } from "meteor/react-meteor-data";
import { fade, withStyles } from "@material-ui/core/styles";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import AccountsUIWrapper from "./AccountsUIWrapper.js";

import LangMenu from "./LangMenu";

const styles = theme => ({
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto"
    }
  },
  searchIcon: {
    width: theme.spacing(7),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  inputRoot: {
    color: "inherit"
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: 200
    }
  }
});

class Bar extends BaseComponent {
  constructor(props) {
    super(props);
    this.handleFilterChange = this.handleFilterChange.bind(this);
  }
  handleFilterChange(event) {
    this.props.onFilterChange(event.target.value);
  }
  render() {
    const { classes, username } = this.props;
    return (
      <AppBar position="fixed">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            {i18n.__("components.Bar.appTitle")} (
            {username ? username : i18n.__("components.Bar.guestUser")})&nbsp;
          </Typography>
          <AccountsUIWrapper />
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput
              }}
              inputProps={{ "aria-label": "search" }}
              value={this.props.filterText}
              onChange={this.handleFilterChange}
            />
          </div>
          <LangMenu />
        </Toolbar>
      </AppBar>
    );
  }
}

const BarWithTracker = withTracker(() => {
  return {
    username: Meteor.user() ? Meteor.user().username : null
  };
})(Bar);
const BarWithStyles = withStyles(styles)(BarWithTracker);
export { BarWithStyles as Bar };
