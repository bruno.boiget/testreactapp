import React from "react";
import BaseComponent from "./BaseComponent.jsx";
import i18n from "meteor/universe:i18n";
import PropTypes from "prop-types";
import { Meteor } from "meteor/meteor";
import { withTracker } from "meteor/react-meteor-data";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Links from "../api/links";
import { LinkCard } from "./LinkCard";

const styles = theme => ({
  content: {
    overflow: "hidden",
    padding: 8,
    marginBottom: 56
  }
});

class ListLinks extends BaseComponent {
  render() {
    const { classes } = this.props;
    const Links = this.props.links.map(link => this.makeLink(link));

    return (
      <div className={classes.content}>
        <Grid
          container
          spacing={4}
          direction="row"
          justify="center"
          alignItems="center"
        >
          {this.props.loading ? (
            <span>{i18n.__("components.ListLinks.loading")}</span>
          ) : (
            Links
          )}
        </Grid>
      </div>
    );
  }

  makeLink(link) {
    let searchText = link.title + link.description;
    searchText = searchText.toLowerCase();
    if (
      this.props.filterText &&
      searchText.indexOf(this.props.filterText.toLowerCase()) == -1
    ) {
      return;
    }
    return (
      <Grid item key={link._id}>
        <LinkCard _key={link._id} link={link} />
      </Grid>
    );
  }
}

ListLinks.propTypes = {
  links: PropTypes.array,
  loading: PropTypes.bool
};

const ListLinksWithTracker = withTracker(() => {
  const linksHandle = Meteor.subscribe("links");
  const loading = !linksHandle.ready();
  const links = Links.find({}, { sort: { title: 1 } }).fetch();
  return {
    links,
    loading
  };
})(ListLinks);
const ListLinksWithStyles = withStyles(styles)(ListLinksWithTracker);
export { ListLinksWithStyles as ListLinks };
