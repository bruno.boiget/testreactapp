import React from "react";
import BaseComponent from "./BaseComponent.jsx";
import { Route, BrowserRouter, Switch } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import { ListLinks } from "./ListLinks";
import { Bar } from "./Bar";
import { Nav } from "./Nav";
import About from "./About";
import { FormLink } from "./FormLink";
import { LinkDetail } from "./LinkDetail";

const styles = theme => ({
  root: {},
  filler: {
    height: 64
  }
});

class AppContainer extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = Object.assign(this.state, { filterText: "" });
    this.handleFilterChange = this.handleFilterChange.bind(this);
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <CssBaseline />
        <BrowserRouter>
          <Bar
            filterText={this.state.filterText}
            onFilterChange={this.handleFilterChange}
          />
          <div className={classes.filler}></div>
          <Switch>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/card/:linkId" component={LinkDetail} />
            <Route path="/">
              <FormLink />
              <ListLinks filterText={this.state.filterText} />
            </Route>
          </Switch>
          <Nav />
        </BrowserRouter>
      </div>
    );
  }
  handleFilterChange(filterText) {
    this.setState({ filterText: filterText });
  }
}

AppContainerWithStyles = withStyles(styles)(AppContainer);
export { AppContainerWithStyles as AppContainer };
