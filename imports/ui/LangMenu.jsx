import React from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import i18n from "meteor/universe:i18n";
import BaseComponent from "./BaseComponent.jsx";
import getLanguages from "../api/languages/methods";

class LangMenu extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = Object.assign(this.state, { languages: [], anchorEl: null });
    this.handleClick = this.handleClick.bind(this);
    this.setLocale = this.setLocale.bind(this);
  }

  componentDidMount() {
    getLanguages.call((error, languages) => {
      if (!error) {
        this.setState({
          languages
        });
      }
    });
  }

  handleClick(event) {
    this.setState({ anchorEl: event.currentTarget });
  }

  setLocale(event, language) {
    event.preventDefault();
    if (language) {
      i18n.setLocale(language);
    }
    this.setState({ anchorEl: null });
  }

  renderLanguages() {
    return this.state.languages.map(language => {
      return (
        <MenuItem
          key={language}
          onClick={event => this.setLocale(event, language)}
        >
          {language}
        </MenuItem>
      );
    });
  }

  render() {
    return (
      <div>
        <Button
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={this.handleClick}
          color="primary"
          variant="contained"
        >
          {i18n.getLocale()}
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={this.state.anchorEl}
          keepMounted
          open={Boolean(this.state.anchorEl)}
          onClose={event => this.setLocale(event, null)}
        >
          {this.renderLanguages()}
        </Menu>
      </div>
    );
  }
}

export default LangMenu;
