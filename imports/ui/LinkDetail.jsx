import React from "react";
import BaseComponent from "./BaseComponent.jsx";
import { withTracker } from "meteor/react-meteor-data";

class LinkDetail extends BaseComponent {
  render() {
    const { loading, link } = this.props;
    return (
      <div>
        {loading ? (
          <span>Loading...</span>
        ) : (
          <div>
            <span>LinkDetails works !</span>
            <p>ID : {link._id}</p>
            <p>Title : {link.title}</p>
            <p>Description : {link.description}</p>
            <p>URL : {link.url}</p>
          </div>
        )}
      </div>
    );
  }
}

const LinkDetailWithTracker = withTracker(({ match }) => {
  const { linkId } = match.params;
  const linkHandle = Meteor.subscribe("link", { linkId });
  const loading = !linkHandle.ready();
  const link = Links.findOne(linkId);
  return {
    link,
    loading
  };
})(LinkDetail);
export { LinkDetailWithTracker as LinkDetail };
