import React from "react";
import BaseComponent from "./BaseComponent.jsx";
import i18n from "meteor/universe:i18n";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import Slide from "@material-ui/core/Slide";

import { removeLink } from "../api/methods";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const styles = theme => ({
  card: {
    width: 200,
    height: 480,
    display: "flex",
    "flex-direction": "column",
    "justify-content": "space-between"
  }
});

class LinkCard extends BaseComponent {
  constructor(props) {
    super(props);
    this.handleRemove = this.handleRemove.bind(this);
    this.confirmRemove = this.confirmRemove.bind(this);
    this.gotoLink = this.gotoLink.bind(this);
    this.handleCancelRemove = this.handleCancelRemove.bind(this);
    this.state = Object.assign(this.state, { dialogOpen: false });
  }

  confirmRemove() {
    this.setState({ dialogOpen: true });
  }

  handleCancelRemove() {
    this.setState({ dialogOpen: false });
  }

  handleRemove() {
    this.setState({ dialogOpen: false });
    removeLink.call({ linkId: this.props.link._id }, (err, res) => {
      if (err) {
        if (err.error === "Links.methods.not-logged-in") {
          Bert.alert(
            i18n.__("components.Link.notifyNotLoggedin"),
            "danger",
            "growl-top-right"
          );
          return;
        }
        Bert.alert(err.reason, "danger", "growl-top-right");
      } else {
        Bert.alert(
          i18n.__("components.Link.notifyRemoved"),
          "success",
          "growl-top-right"
        );
      }
    });
  }

  gotoLink() {
    console.log(this.props);
    // redirect to /card/linkId
    let linkId = this.props.link._id;
    this.props.history.push("/card/" + linkId);
  }

  render() {
    const { classes, _key, link } = this.props;
    return (
      <div>
        <Card id={_key} className={classes.card}>
          <CardActionArea onClick={this.gotoLink}>
            <CardMedia
              component="img"
              alt={link.title}
              image={link.url}
              title={link.title}
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                {link.title}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                {link.description}
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button size="small" color="primary" onClick={this.gotoLink}>
              {i18n.__("components.Link.details")}
            </Button>
            <Button size="small" color="primary" onClick={this.confirmRemove}>
              {i18n.__("components.Link.remove")}
            </Button>
          </CardActions>
        </Card>
        <Dialog
          open={this.state.dialogOpen}
          TransitionComponent={Transition}
          keepMounted
          onClose={this.handleCancelRemove}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              {i18n.__("components.Link.confirmRemove")}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCancelRemove} color="primary">
              {i18n.__("components.Link.cancelRemove")}
            </Button>
            <Button onClick={this.handleRemove} color="primary">
              {i18n.__("components.Link.remove")}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

const LinkCardWithStyles = withStyles(styles)(LinkCard);
const LinkCardWithRouter = withRouter(LinkCardWithStyles);
export { LinkCardWithRouter as LinkCard };
