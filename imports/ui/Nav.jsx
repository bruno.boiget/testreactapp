import React from "react";
import BaseComponent from "./BaseComponent.jsx";
import i18n from "meteor/universe:i18n";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import AppsIcon from "@material-ui/icons/Apps";
import InfoIcon from "@material-ui/icons/Info";

const styles = theme => ({
  nav: {
    position: "fixed",
    bottom: 0,
    width: 180,
    left: "50%",
    transform: "translate(-50%, 0)"
  }
});

class Nav extends BaseComponent {
  constructor(props) {
    super(props);
    this.navigate = this.navigate.bind(this);
    this.state = Object.assign(this.state, {
      value: props.location.pathname === "/about" ? 1 : 0
    });
  }

  navigate(event, newValue) {
    const choices = {
      0: "/",
      1: "/about"
    };
    this.setState({ value: newValue });
    this.props.history.push(choices[newValue]);
  }

  render() {
    const { classes } = this.props;
    return (
      <BottomNavigation
        value={this.state.value}
        onChange={this.navigate}
        showLabels
        className={classes.nav}
      >
        <BottomNavigationAction
          label={i18n.__("components.Nav.mainPage")}
          icon={<AppsIcon />}
        />
        <BottomNavigationAction
          label={i18n.__("components.Nav.aboutPage")}
          icon={<InfoIcon />}
        />
      </BottomNavigation>
    );
  }
}

const NavWithStyles = withStyles(styles)(Nav);
const NavWithRouter = withRouter(NavWithStyles);
export { NavWithRouter as Nav };
