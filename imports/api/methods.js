import i18n from "meteor/universe:i18n";
//import { check } from 'meteor/check';
//import { Match } from 'meteor/check';
import { ValidatedMethod } from "meteor/mdg:validated-method";

export const insertLink = new ValidatedMethod({
  name: "Links.methods.insert",
  validate: new SimpleSchema({
    title: { type: String, min: 1 },
    description: { type: String, min: 1 },
    url: { type: String, min: 1 }
  }).validator(),
  run(newLink) {
    // In here, we can be sure that the newLink argument is
    // validated.
    if (!this.userId) {
      throw new Meteor.Error(
        "Links.methods.not-logged-in",
        i18n.__("api.links.mustBeLoggedIn")
      );
    }

    Links.insert(newLink);
  }
});

export const removeLink = new ValidatedMethod({
  name: "Links.methods.remove",
  validate: new SimpleSchema({
    linkId: { type: String, min: 1 }
  }).validator(),
  run({ linkId }) {
    if (!this.userId) {
      throw new Meteor.Error(
        "Links.methods.not-logged-in",
        i18n.__("api.links.mustBeLoggedIn")
      );
    }
    // check if Link exists
    if (!Links.findOne(linkId)) {
      throw new Meteor.Error(
        "Links.methods.unknwon-link",
        i18n.__("api.links.unknownLink")
      );
    }

    Links.remove(linkId);
  }
});

//const NonEmptyString = Match.Where((x) => {
//    check(x, String);
//    return x.length > 0;
//});

// Meteor.methods({
//     'links.insert'(title, description, url) {
//       check(title, NonEmptyString);
//       check(description, NonEmptyString);
//       check(url, NonEmptyString);
//       // Make sure the user is logged in before inserting a task
//       if (! this.userId) {
//         throw new Meteor.Error('not-authorized');
//       }

//       Links.insert({
//         title,
//         description,
//         url
//       });
//     },
// });
