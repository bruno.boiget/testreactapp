import { Mongo } from "meteor/mongo";
import { Meteor } from "meteor/meteor";

export default Links = new Mongo.Collection("links");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("links", function linksPublication() {
    return Links.find();
  });
  Meteor.publish("link", function linkPublication(params) {
    new SimpleSchema({
      linkId: { type: String }
    }).validate(params);
    const { linkId } = params;
    console.log("publishing link with ID ", linkId);
    return Links.find({ _id: linkId });
  });
}
