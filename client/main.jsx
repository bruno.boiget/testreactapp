import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import i18n from 'meteor/universe:i18n';

import '../imports/startup/accounts-config.js';
import App from '/imports/ui/App'

function getLang () {
  return (
      navigator.languages && navigator.languages[0] ||
      navigator.language ||
      navigator.browserLanguage ||
      navigator.userLanguage ||
      'en-US'
  );
}

Meteor.startup(() => {
  // set locale to navigator default at startup
  i18n.setLocale(getLang());
  render(<App />, document.getElementById('react-target'));
});
